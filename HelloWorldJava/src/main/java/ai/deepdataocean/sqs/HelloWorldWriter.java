package ai.deepdataocean.sqs;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.CreateQueueRequest;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.amazonaws.services.sqs.model.SendMessageResult;


public class HelloWorldWriter implements RequestHandler<Object, Object> {

    private static final String QUEUE_NAME = "HelloWorldQueue";
    private static  final String AWS_REGION = "us-east-2";

    @Override
    public Object handleRequest(Object input, Context context) {
        return sendMessage(String.valueOf(input), context);
    }

    /**
     * @param message
     * @param context
     * @return resultMessage
     */
    private String sendMessage(String message, Context context) {
        AmazonSQS amazonSQS = AmazonSQSClientBuilder.standard()
                .withRegion(AWS_REGION)
                .build();
        CreateQueueRequest createQueueRequest = new CreateQueueRequest(QUEUE_NAME);
        String queueUrl = amazonSQS.createQueue(createQueueRequest).getQueueUrl();
        context.getLogger().log("Sending message : '" + message + "' to QUEUE = '" + QUEUE_NAME + "' to QUEUE URL : " + queueUrl);
        SendMessageResult sendMessageResult = amazonSQS.sendMessage(new SendMessageRequest()
                .withQueueUrl(queueUrl)
                .withMessageBody(message));
        return "Sending of message succeeded with messageId : " + sendMessageResult.getMessageId();
    }
}