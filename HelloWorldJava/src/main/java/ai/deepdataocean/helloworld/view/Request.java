package ai.deepdataocean.helloworld.view;

public class Request {

    private String message;

    public Request() {
    }

    public void setMessage(final String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
