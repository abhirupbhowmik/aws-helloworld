package ai.deepdataocean.helloworld.service;

public class HelloWorldService {

    public String generateHello(final String message, final String userName) {
        return String.format("%s from %s", message, userName);
    }
}
