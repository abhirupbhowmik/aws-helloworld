package ai.deepdataocean.helloworld;


import ai.deepdataocean.helloworld.service.HelloWorldService;
import ai.deepdataocean.helloworld.view.Request;
import ai.deepdataocean.helloworld.view.Response;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

public class HelloWorldLambdaHandler implements RequestHandler<Request, Response> {

    private final HelloWorldService helloWorldService;

    public HelloWorldLambdaHandler() {
        this.helloWorldService = new HelloWorldService();
    }

    public Response handleRequest(Request request, Context context) {
        /* Printing context objects */
        context.getLogger().log("AwsRequestId : " + context.getAwsRequestId());
        context.getLogger().log("FunctionName : " + context.getFunctionName());
        context.getLogger().log("FunctionVersion : " + context.getFunctionVersion());
        context.getLogger().log("InvokedFunctionArn : " + context.getInvokedFunctionArn());

        final String message = request.getMessage();
        String user = System.getenv("USERPROFILE");
        final String userName = user.substring(user.lastIndexOf('\\') + 1, user.length());
        return new Response(helloWorldService.generateHello(message, userName));
    }
}