package ai.deepdataocean.sqs;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.SQSEvent;

import java.util.List;

public class HelloWorldLogger implements RequestHandler<SQSEvent, Void> {
    @Override
    public Void handleRequest(SQSEvent event, Context context) {
        try {
            List<SQSEvent.SQSMessage> sqsMessages = event.getRecords();
            context.getLogger().log("Message from HelloWorldLogger : " + sqsMessages.get(0).getBody());
        } catch (Exception exception) {
            context.getLogger().log("Exception occurred while reading message from HelloWorldLogger SQS Queue : " + exception.getMessage());
            throw exception;
        }
        return null;
    }
}
